package com.example.firsthomeworkag

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        val generateNumberButton = findViewById<Button>(R.id.generateNumberButton)
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumberTextView)
        val yesOrNoTextView = findViewById<TextView>(R.id.yesOrNoTextView)
        generateNumberButton.setOnClickListener {
            val number: Int = randomNumber()
            d("generateNumber", "This is a Random Number ${devidedByFivePositive(number)}")
            randomNumberTextView.text = number.toString()
            yesOrNoTextView.text = devidedByFivePositive(number)



        }
    }
    private fun randomNumber() = (-100..100).random()


    fun devidedByFivePositive(randomNumber: Int): String{
        if (randomNumber%5==0 && randomNumber/5>0){
            return "Yes"
        }else{
            return "No"
        }
    }




}